﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    public GameObject bullet;
    public Text score;
    public float speed = 0.01f;
    // Use this for initialization
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {



        float x = Input.GetAxisRaw("Horizontal");
        GetComponent<Rigidbody2D>().velocity = new Vector2(x, 0) * speed;



        if (Input.GetKeyDown(KeyCode.Space))
        {

            CreateBullet();
        }
    }

    
    public void CreateBullet()
    {
        Instantiate(bullet, transform.position, Quaternion.identity);

    }
    

}
