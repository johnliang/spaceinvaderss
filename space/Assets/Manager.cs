﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Manager : MonoBehaviour {
    public Text score;
    public int PlayerScore;
    // Use this for initialization
    void Start () {
        PlayerScore = 0;
    }
	
	// Update is called once per frame
	void Update () {
        score.text = "Score" + PlayerScore;

        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel(0);
        }
	}
}
