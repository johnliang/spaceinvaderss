﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class bullet : MonoBehaviour
{
    public int speed;
    public Text score;
    public int PlayerScore;


    // Use this for initialization
    void Start()
    {

        PlayerScore = 0;
        speed = 5;
        GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score" + PlayerScore;
        float k = Input.GetAxisRaw("Vertical");
        //GetComponent<Rigidbody2D>().velocity = new Vector2(0, k) * speed;
    }



    void OnTriggerEnter2D(Collider2D other)
    {
        string layerName = LayerMask.LayerToName(other.gameObject.layer);

        if (layerName == "Enemy")
        {
            Destroy(other.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Enemy")

        {
            Destroy(gameObject);
        }
    }
}
